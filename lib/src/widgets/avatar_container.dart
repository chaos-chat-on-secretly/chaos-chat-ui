part of chaos_chat_ui;

/// Avatar container for the the chat view uses a [CircleAvatar]
/// widget as default which can be overriden by providing
/// [avatarBuilder] property
class AvatarContainer extends StatefulWidget {
  /// A [ChatUser] object use to get the url of the user
  /// avatar
  final ChatUser user;

  /// [onPress] function takea a function with this structure
  /// [Function(ChatUser)] will trigger when the avatar
  /// is tapped on
  final Function(ChatUser)? onPress;

  /// [onLongPress] function takea a function with this structure
  /// [Function(ChatUser)] will trigger when the avatar
  /// is long pressed
  final Function(ChatUser)? onLongPress;

  /// [avatarBuilder] function takea a function with this structure
  /// [Widget Function(ChatUser)] to build the avatar
  final Widget Function(ChatUser)? avatarBuilder;

  /// [constraints] to apply to build the layout
  /// by default used MediaQuery and take screen size as constaints
  final BoxConstraints? constraints;

  final double? avatarMaxSize;

  AvatarContainer({
    required this.user,
    this.onPress,
    this.onLongPress,
    this.avatarBuilder,
    this.constraints,
    this.avatarMaxSize,
  }) : super(key: ValueKey(user.avatar));

  @override
  _AvatarContainerState createState() => _AvatarContainerState();
}

class _AvatarContainerState extends State<AvatarContainer> {
  NetworkImage? _image;
  bool _checkLoaded = true;

  @override
  void initState() {
    if (widget.user.avatar != null && widget.user.avatar!.length != 0) {
      _image = new NetworkImage(widget.user.avatar!);
      _image!
          .resolve(new ImageConfiguration())
          .addListener(ImageStreamListener((ImageInfo info, bool _) {
        if (mounted) {
          setState(() {
            _checkLoaded = false;
          });
        }
      }));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.onPress != null ? widget.onPress!(widget.user) : null,
      onLongPress: () =>
          widget.onLongPress != null ? widget.onLongPress!(widget.user) : null,
      child: widget.avatarBuilder != null
          ? widget.avatarBuilder!(widget.user)
          : CircleAvatar(
              radius: 20,
              foregroundImage: !_checkLoaded ? _image : null,
              backgroundColor: !_checkLoaded
                  ? Theme.of(context).colorScheme.background
                  : Colors.grey,
              child: !_checkLoaded
                  ? Text("")
                  : Text(widget.user.name == null || widget.user.name!.isEmpty
                      ? ''
                      : widget.user.name![0])),
    );
  }
}
