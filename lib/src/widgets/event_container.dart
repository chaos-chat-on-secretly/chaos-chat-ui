part of chaos_chat_ui;

class EventContainer extends StatelessWidget {
  final String? eventtext;

  EventContainer({
    this.eventtext,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(3),
        margin: EdgeInsets.symmetric(vertical: 10.0),
        decoration: BoxDecoration(
          color: Colors.transparent,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Text(
          eventtext ?? "An Unknown Event Occurred!",
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Theme.of(context).hintColor,
              fontSize: 12.0,
              fontStyle: FontStyle.italic),
        ));
  }
}
