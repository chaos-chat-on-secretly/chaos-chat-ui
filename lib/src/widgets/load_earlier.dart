part of chaos_chat_ui;

class LoadEarlierWidget extends StatelessWidget {
  const LoadEarlierWidget({
    Key? key,
    required this.onLoadEarlier,
    required this.defaultLoadCallback,
  }) : super(key: key);

  final Function onLoadEarlier;
  final Function(bool)? defaultLoadCallback;

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: () {
        onLoadEarlier();
        defaultLoadCallback!(false);
      },
      child: Text("Load earlier messages"),
    );
  }
}
